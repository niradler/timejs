# timejs
Helper class to help manage time base function.

# Getting Started
- server: [demo source](https://github.com/niradler/timejs/blob/master/examples.js)
- client: [source](https://github.com/niradler/timejs/blob/master/index.html) [demo](https://niradler.github.io/timejs/)
